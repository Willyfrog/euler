# autor: guillermo vaya
# problema: http://projecteuler.net/problem=1

def multiplo3o5(x):
    '''
        dado un valor X, mostrar los valores multiplos de 3 o de 5 sin incluir
        a la x
    '''
    treses = set([t*3 for t in range(1,(x-1)/3+1)])
    #print "treses: %r" % treses
    cincos = set([t*5 for t in range(1,(x-1)/5+1)])
    #print "cincos: %r" % cincos
    print "total: %r" % treses
    return treses | cincos #union

if __name__=='__main__':
    v = 0
    x = 1000
    for m in multiplo3o5(x):
        v = v+m
    print v
