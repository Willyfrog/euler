# autor: Guillermo Vaya
# problema 2: http://projecteuler.net/problem=2

def fibonacci(x):
    '''
    serie de fibonacci hasta x
    '''
    fib = [1,1]
    while (fib[-1]<=x):
        fib.add(fib[-1]+fib[-2])
    return fib

def suma_fibo(x):
    '''
    suma los pares de una serie de fibonacci hasta x
    '''
    #fib = fibonacci(x)
    fib = [0,1]
    y = 0
    total = 0
    while y <= x:
        y = fib[0] + fib[1] #calculamos
        fib[0] = fib[1] #movemos
        fib[1] = y
        total += ((y+1)%2)*y #sumamos si es par
    return total
    
if __name__=='__main__':
    print (suma_fibo(4000000))
