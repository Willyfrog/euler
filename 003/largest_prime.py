#autor: Guillermo Vaya
#problema 3: http://projecteuler.net/problem=3

def gen_primos(maximo):
    primos = []
    x=1
    while x<maximo:
        x += 1
        es_p = True
        for i in primos:
            es_p = es_p and (x%i > 0)
        if es_p:
            primos.append(x)
            #print "nuevo primo %s " % x
            yield(x)


def es_primo(x):
    '''comprueba si es primo'''
    res = True
    if x>1: #no es optimo, pero es suficientemente bueno
        for i in xrange(2,x/2):
            res = res and (x%i>0)
            if not res:
                break
    return res

def lp(x):
    gp = gen_primos(x/2)
    y = 0
    maxy = 0
    while y <= x/2:
        y=gp.next()
        if x%y==0:
            maxy = y
            print "encontrado: %s" % y
    return maxy


def largest_prime(x):
    '''devuelve el primo mayor'''
    #gp = gen_primos()
    #maxp = 1
    #p = gp.next()
    #while (p < x/2):
    #    if (x%p == 0):
    #        print "por ahora: %s" % p 
    #        maxp = p
    #    p=gp.next()
    #return maxp
    if es_primo(x):
        res = x
    else:
        for i in xrange(x/2,0,-1):
            if (x%i==0):
                print "multiplo %s" % i
                if es_primo(i):
                    print "encontrado: %s" % i
                    res = i
                    break

    return res
if __name__=='__main__':
    print  (lp(600851475143))
    #print (largest_prime(600851475143))
