#autor: Guillermo Vaya
#problema 4: http://projecteuler.net/problem=4
#
# Find the largest palindrome made from the product of two 3-digit numbers.

def check_pal(pal):
    '''check if it's a palindrome'''
    spal = str(pal) #conversion to string
    return (spal==spal[::-1])

# numero maximo generado por 2 numeros de 3 digitos: 999*999 = 998001
# numero minimo generado por 2 numeros de 3 digitos: 100*100 = 10000
# generar palindromos posibles
def pal_gen_6():
    '''generador de palindromos de 6 digitos'''
    for a in xrange(9,-1,-1):
        for b in xrange(9,-1,-1):
            for c in xrange(9,-1,-1):
                yield a*100001+b*10010+c*1100

def pal_gen_5():
    '''generador de palindromos de 5 digitos'''
    for a in xrange(9,-1,-1):
        for b in xrange(9,-1,-1):
            for c in xrange(9,-1,-1):
                yield a*10001+b*1010+c*100

# encontrar divisores de 3 digitos
def divisor3(x):
    res1=0
    for a in range(999,99,-1):
        if x%a==0:
            res1=a
            res2=x/a
            print "%s es divisible por %s y %s" % (x,res1,res2)
            if len(str(res2))==3:
                break
            else:
                res1=0
                print "%s no tiene 3 digitos" % res2
    if not res1:
        print "%s no tiene 2 divisores de 3 digitos" % x
    return res1

if __name__=='__main__':
    p6 = pal_gen_6()
    res = 0
    for p in p6:
        res=divisor3(p)
        if res:
            break
    if not res:
        print "probando los de 5 digitos"
        p5 = pal_gen_5()
        for p in p5:
            res=divisor3(p)
            if res:
                break
    print "resultado: %s" % res
