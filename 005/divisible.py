#autor: Guillermo Vaya
#problema 5: http://projecteuler.net/problem=5

# What is the smallest positive number that is evenly divisible by all of the
# numbers from 1 to 20?

if __name__=='__main__':
    res = 1
    multiplos = []
    for i in range(2,21):
        if (res % i) != 0:
            mul = i
            for i in multiplos:
                if mul % i ==0:
                    mul /= i
            multiplos.append(mul)
            res *= mul

    print res
    print multiplos

