# autor: Guillermo Vaya
# problema 6: http://projecteuler.net/problem=6

# Find the difference between the sum of the squares of the first one hundred
# natural numbers and the square of the sum.

if __name__=='__main__':
    s1 = 0 #suma de cuadrados
    s2 = 0 #cuadrado de la suma
    for i in xrange(1,101):
        s1 += i**2
        s2 += i
    s2 = s2**2
    res = s2 - s1
    print "%s - %s = %s" % (s1, s2, res)
