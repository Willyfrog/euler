#autor: Guillermo Vaya
#problema 7: http://projecteuler.net/problem=7

# What is the 10 001st prime number?

# sacado del problema 3 y modificado
def gen_primos(n):
    primos = []
    x=1
    while len(primos)<n: 
        x += 1
        es_p = True
        for i in primos:
            es_p = es_p and (x%i > 0)
        if es_p:
            primos.append(x)
            print "%s primo %s " % (len(primos),x)
            #yield(x)
    return primos[-1]

if __name__=='__main__':
#    print gen_primos(6)
    print gen_primos(10001)
