#autor: Guillermo Vaya
#problema 10: http://projecteuler.net/problem=10

# Find the sum of all the primes below two million.

# sacado del problema 3
from math import sqrt
def gen_primos(maximo):
    primos = []
    x=1
    while x<maximo:
        x += 1
        es_p = True
        for i in primos:
            es_p = es_p and (x%i > 0)
        if es_p:
            primos.append(x)
            #print "nuevo primo %s " % x
            yield(x)

# algoritmo de la criba de eratostenes
# http://es.wikipedia.org/wiki/Criba_de_Erat%C3%B3stenes
def eratostenes(m):
    primos = set(range(2,m+1))
    for i in range(2,int(sqrt(m))+1):
        if i in primos:
            print "encontrado %s " % i 
            for j in range(2,m/i+1):
                primos.discard(i*j)
    return primos



if __name__=='__main__':
#    gp = gen_primos(2000000)
#    gp = gen_primos(10)
#    res = 0
#    for i in gp:
#        res+=i
#        print  "%s /t/t %s" % (res,i)
    primos = eratostenes(2000000)
    res=0
    while primos:
        res+=primos.pop()
    print res
