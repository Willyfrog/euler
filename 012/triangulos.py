# autor: guillermo vaya
# problema 12
# What is the value of the first triangle number to have over five hundred
# divisors?
# los numeros triangulares son aquellos sacados por sumar los numeros
# naturales: 1,3,6,10....
from itertools import count
from math import sqrt


def gen_triangular():
    ''' generador de numeros triangulares'''
    t = 0
    for i in count():
        t += i
        yield t

# modificacion de generador de primos previo
def gen_primos():
    primos = []
    x=1
    for x in count():
        x += 1
        es_p = True
        for i in primos:
            es_p = es_p and (x%i > 0)
        if es_p:
            primos.append(x)
            #print "nuevo primo %s " % x
            yield(x)

# algoritmo de la criba de eratostenes
# http://es.wikipedia.org/wiki/Criba_de_Erat%C3%B3stenes
def eratostenes(m):
    primos = set(range(2,m+1))
    for i in xrange(2,int(sqrt(m))+1):
        if i in primos:
#            print "encontrado %s " % i 
            for j in xrange(2,m/i+1):
                primos.discard(i*j)
    return primos

def erato_follow(m,primes):
    '''
        m   - new top
        old - previous list of prime numbers
    '''
    if not primes:
        pl = eratostenes(m)
    else:
        pl = primes
        np = set(range(max(pl),m)) #nuevos primos
        for i in xrange(2,int(sqrt(m))+1):
            if i < max(pl): #aprovechamos lo aprendido
                for j in xrange(2,m/i+1):
                    np.discard(i*j)
            else: #sacamos los que quedan
                for j in xrange(2,m/i+1):
                    np.discard(i*j)
        return pl | np # union de ambos


def divisores(n,primos):
    ''' saca los divisores de un numero'''
#    primos = eratostenes(n)
    divs = 1
    for i in primos:
        k = n
        pdivs=0
        while (k > 1) and (k % i==0):
            k /= i
            pdivs +=1
        if pdivs:
            divs*=(pdivs+1)
        if i>sqrt(n):
            break
    return divs


if __name__=='__main__':
    top = 100000
    primos = eratostenes(top)
    tri = gen_triangular()
    buf = 0
    for t in tri:
        if top < sqrt(t): #estaba teniendo problemas con la generacion de primos, asi
                    # que la agilizo evitando el recalculo a cada numero, si se
                    # queda corto, dobla el tope y recalcula los primos
            top *=2
#            print "recalculando primos para %s" % top
            primos = erato_follow(top,primos)
        d = divisores(t, primos)
        if d > buf: #esto permite tener un cierto tracking de como avanza y no
                    # encontrarte ante una cli que no hace nada, pero sin
                    # apabullar a numeros
#            print "t: %s, tiene %s divisores" % (t,d)
            buf = d
        if d > 500:
            print "t: %s, tiene %s divisores" % (t,d)
            break
