# autor: guillermo vaya
# problema: 14 http://projecteuler.net/problem=14
# Serie:
# n -> n/2 (n is even)
# n -> 3n + 1 (n is odd)
# It is supposed to end at 1
# Which starting number, under one million, produces the longest chain?

saltos = {1:1}

def siguiente(n):
    '''
    funcion para seguir la serie dado un numero n
    '''
    if not n%2:
        r = n/2
    else:
        r = 3*n+1
    return r

def salta(n):
    if not saltos.has_key(n):
        saltos[n] = salta(siguiente(n))+1
    return saltos[n]

if __name__=='__main__':
    maxhops = 0
    maxnum = 1
    for i in xrange(2,1000000):
        hops = salta(i)
        if hops > maxhops:
            maxnum = i
            maxhops = hops
            print "%s es el que mas salta con %s saltos" % (maxnum,maxhops)
        #print "%s tiene un total de %s saltos" % (i,hops)
    print "%s es el que mas salta con %s saltos" % (maxnum,maxhops)


    
