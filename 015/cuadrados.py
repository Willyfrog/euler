#autor: guillermo vaya
# problema 15 http://projecteuler.net/problem=15
# starting in the top left corner of a 2*2 grid, there are 6 routes (without
# backtracking) to the bottom right corner.


def calcula(i,j):
    '''
    formula de calculo:
    es la suma de las soluciones anteriores si toma el camino horizontal y
    vertical. Los bordes solo tienen un camino=1
    '''
    print " calcula( %s, %s)" % (i,j)
    if cuadrados[i][j]==0: #si no hemos pasado por aqui
        if (i>0 and j>0): #los laterales siempre son 1
            print "no hay 0, sumo"
            arr = calcula(i-1,j)
            dch = calcula(i,j-1)
            if not arr or not dch:
                print ("cuidado, una ha dado 0 y eso es imposible %s, %s" %
                (arr,dch))
            print "(%s, %s) %s + %s = %s" %(i,j,arr,dch,arr+dch)
            cuadrados[i][j]=arr + dch
        else:
            print "es 0"
            cuadrados[i][j]=1
    else:
        print "ya calculado %s" % cuadrados[i][j]

    return cuadrados[i][j]

if __name__ == '__main__':
    cuadrados = [[0 for i in range(21)] for j in range(21)]
#    for i in range(len(cuadrados)):
#        for j in range(len(cuadrados[0])):
#            print "%s, %s: %s" % (i,j,calcula(i,j))
    print "resultado %s" % calcula(20,20)
#    print calcula(19,19)
    for i in cuadrados:
        t = ""
        for j in i:
            t += "%011s " % j #abrir bien la pantalla que ocupa
        print t

    
