# autor: Guillermo Vaya
# resolviendo el arbol de navidad de qbert
# http://united-coders.com/christian-harms/qbert-chrismas-tree-puzzle
import urllib
tree = [[75],
        [95,64],
        [17,47,82],
        [18,35,87,10],
        [20, 4,82,47,65],
        [19, 1,23,75, 3,34]]

tri = []
peUrl = "http://projecteuler.net/project/triangle.txt"

tri18=[[75],
    [95, 64],
    [17, 47, 82],
    [18, 35, 87, 10],
    [20, 04, 82, 47, 65],
    [19, 01, 23, 75, 03, 34],
    [88, 02, 77, 73, 07, 63, 67],
    [99, 65, 04, 28, 06, 16, 70, 92],
    [41, 41, 26, 56, 83, 40, 80, 70, 33],
    [41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
    [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
    [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
    [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
    [63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
    [04, 62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60, 04, 23]]


def solve_line(curr, prev):
    """
        given 2 lines: max(curr[i]+prev[i],curr[i]+prev[i+1])
    """
    if not prev:
        res=curr
    else:
        res=[max(prev[i],prev[i+1])+curr[i] for i in range(len(curr))]
    return res

def qbert(arbol):
    """
        dada una lista en forma de arbol (ver variable tree) resolver 
        la suma maxima pudiendo solo bajar bien a izda. o dcha.
    """
    arbol.reverse() #invertimos la lista
    res = []
    for l in arbol:
        res = solve_line(l,res)
    return res

if __name__=='__main__':
    print "solucion al arbolito:"
    print qbert(tree)
    for line in urllib.urlopen(peUrl).readlines():
        tri.append([int(x) for x in line.strip().split(" ")])
    print "solucion al arbolote:"
    print qbert(tri)
    print "solucion al problema 18"
    print qbert(tri18)
