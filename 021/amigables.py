#autor guillermo vaya
# problema 21

#Let d(n) be defined as the sum of proper divisors of n (numbers less than n
# which divide evenly into n).
#If d(a) = b and d(b) = a, where a != b, then a and b are an amicable pair and
# each of a and b are called amicable numbers.
# 
# Evaluate the sum of all the amicable numbers under 10000.

def d(n):
    s = 0
    for i in range(1,(n/2)+1):
        if n%i==0:
            s+=i
    return s

vistos = set()
suma = 0
for i in range(10000):
    if not (i in vistos):
        b = d(i)
        if b<=i:
            continue 
        #no deberiamos habernoslo saltado, no? y tampoco vale si es igual
        if d(b) == i:
            suma+=i
            print ("%s<=>%s"%(i,b))
            vistos.add(b)
            if b<10000:
                suma+=b
        vistos.add(i)

print suma

