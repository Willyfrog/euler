
# autor: Guillermo Vaya
# problema 25: http://projecteuler.net/problem=25


def fibonacci():
    '''
    generador serie de fibonacci
    '''
    p1 = 1
    p2 = 1
    while 1:
        f = p1 + p2
        p1 = p2
        p2 = f
        yield f

if __name__ == '__main__':
    n = 2
    for f in fibonacci():
        n += 1
        if len(str(f)) >= 1000:
            print "%d: %d tiene %d digitos" % (n, f, len(str(f)))
            break
