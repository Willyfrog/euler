# autor: guillermo vaya
# problema 50: http://projecteuler.net/problem=50

# FIXME: no me gusta que haya 2 yields
from math import sqrt
#from collections import deque
import sys
# deque: double ended queue

# sacado del 12
def eratostenes(m):
    primos = set(range(2,m+1))
    for i in xrange(2,int(sqrt(m))+1):
        if i in primos:
#            print "encontrado %s " % i 
            for j in xrange(2,m/i+1):
                primos.discard(i*j)
    return primos


def suma_primos(M):
    primos = eratostenes(M)
#    lista = deque() #almacenaremos los usados como si fueran una cola
    lista = []
    suma = 0
    mlon = 0
    for i in primos:
        if (suma + i) >= M: #nos pasamos?
            while (suma + i) >= M:
                l = lista[0]
                del lista[0]
                #l = lista.popleft()
                suma -=l
                if (suma in primos)and(len(lista)>mlon):
                    mlon = len(lista)
                    #print "%s: %s [%s-%s]" % (suma,mlon,lista[0],lista[-1])
                    yield (suma,mlon,lista)
        lista.append(i)
        suma += i

        if (suma in primos)and(len(lista)>mlon):
            mlon = len(lista)
            #print "%s: %s [%s-%s]" % (suma,mlon,lista[0],lista[-1])
            yield (suma,mlon,lista)

if __name__=='__main__':
    if len(sys.argv) ==2:
        try:
            M = int(sys.argv[1])
        except Exception as e:
            print "solo aceptamos numeros como parametro"
            sys.exit()
    else:
        M=1000000
    for (suma,mlon,lista) in suma_primos(M):
        print "%s: %s" % (suma,mlon)


