# autor: Guillermo Vaya
# resolviendo el arbol de navidad de qbert
# http://united-coders.com/christian-harms/qbert-chrismas-tree-puzzle
import urllib
tree = [[75],
        [95,64],
        [17,47,82],
        [18,35,87,10],
        [20, 4,82,47,65],
        [19, 1,23,75, 3,34]]

tri = []
peUrl = "http://projecteuler.net/project/triangle.txt"


def solve_line(curr, prev):
    """
        given 2 lines: max(curr[i]+prev[i],curr[i]+prev[i+1])
    """
    if not prev:
        res=curr
    else:
        res=[max(prev[i],prev[i+1])+curr[i] for i in range(len(curr))]
    return res

def qbert(arbol):
    """
        dada una lista en forma de arbol (ver variable tree) resolver 
        la suma maxima pudiendo solo bajar bien a izda. o dcha.
    """
    arbol.reverse() #invertimos la lista
    res = []
    for l in arbol:
        res = solve_line(l,res)
    return res

if __name__=='__main__':
    print "solucion al arbolito:"
    print qbert(tree)
    for line in urllib.urlopen(peUrl).readlines():
        tri.append([int(x) for x in line.strip().split(" ")])
    print "solucion al arbolote:"
    print qbert(tri)
